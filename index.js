var express = require('express');
var app = express();

app.get('/', function(req, res){
   var process = processRequest(req, res);
   res.send("parameters in request " + process);
});
  
app.listen(8081, function() {
    console.log('app listening to port 8081')
});

function processRequest(req, res) {
  if (req.query.parms) {
    return req.query.parms;
  }
  return "No prameters";
}
